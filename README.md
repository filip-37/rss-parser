# RSS Atom feed parser

Poznámky k nasadeniu [tu](#deploy).

Po načítaní stránky sa nám zobrazí okno kde je potrebné zadať url adresu rss ktoré chceme parsovať. Počas testovania som skúsil viacero rss feedov a parser správne pracuje pokiaľ ide o Atom, ak nie tak sa výsledok rozparsuje ale nie je zaručené, že sa vykreslí ako je zamýšľané.

Stránka pri pokuse načítať chybnú alebo žiadnu URL vyhodí error upozorňujúci na túto chybu.

Niektoré RSS ako príklad:

- https://www.topky.sk/cl/13/80075/RSS-informacny-servis (Všetky RSS z tejto adresy je možné správne parsovať)
- https://www.sme.sk/rss-title
- https://spravy.pravda.sk/rss/xml/
- https://archive.nytimes.com/www.nytimes.com/services/xml/rss/index.html
- https://www.cbsnews.com/rss/
- https://www.espn.com/espn/news/story?page=rssinfo

Pri parsovaní som použil knižnicu https://github.com/orchestral/parser pre Laravel.

![search](public/images/url.png)

Po načítaní a rozparsovaní RSS sa nám zobrazia požadované informácie. Karta s možnosťou zadania URL zostáva na svojom mieste a tlačidlo "Načítať/Obnoviť" slúži buď na reload pôvodnej URL alebo načítanie novej.

Nižšie pod ňou môžme vidieť kartu s možnosťou zoradenia správ podľa dátumu publikácie.

![sort](public/images/sort.png)

Posledná karta na ľavej strane stránky obsahuje linky na všetky načítané články, zoradené podľa výberu.

Pavá strana webovej stránky obsahuje navrchu názov a popis portála, s ktorého správy pochádzajú, pod ním sa na samostatných kartách nachádzajú všetky články.

![page](public/images/page.png)

Ako môžme vidieť nižšie stránka je responzívna a nemá problém so zobrazenín na žiadnom zariadení (nižšie je uvedené zobrazenie na mobilnom telefóne).

![page](public/images/mobile.png)

## Deploy
RSS Parser som nasadil na Heroku za pomoci Gitlab CI/CD ([pipeline](.gitlab-ci.yml)). V najnovšej verzii je možné karty URL, Zoradiť podľa a Zoznam článkov schovať a rozbaliť kliknutím na vrchnú časť karty, ostatné funkcie ostávajú nezmenené.

* [URL](https://wpub-rss-parser.herokuapp.com/)
* [Repozitár](https://gitlab.com/filip-37/rss-parser)



