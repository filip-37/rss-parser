@extends('layout.app')

@section('content')


    @isset($err)
        <div class="alert alert-danger" role="alert">
            <strong>Chyba!</strong> Zadaná url je buď chybná alebo neexistuje.
        </div>
    @endisset

    <div class="row">
        <div class="col-12 col-md-6">


            <div class="card">
                <div class="card-header" role="tab" id="headingTwo">
                        <span class="custom" data-toggle="collapse" href="#collapseTwo" aria-expanded="true"
                              aria-controls="collapseTwo">
                            URL:
                        </span>
                </div>

                <div id="collapseTwo" class="collapse show" role="tabpanel" aria-labelledby="headingTwo">
                    <div class="card-body use-card">

                        <form action="{{route('load')}}" method="get">
{{--                            {{ csrf_field() }}--}}
                            @if(@isset($url))
                                <input class="form-control" type="text" placeholder="URL" name="url" value="{{$url}}">
                            @else
                                <input class="form-control" type="text" placeholder="URL" name="url" value="">
                            @endif
                            <button class="btn link-btn use-btn">Načítať / Obnoviť</button>
                        </form>
                    </div>
                </div>
            </div>


            @isset($rss)

                <div class="card">
                    <div class="card-header" role="tab" id="headingThree">
                        <span class="custom" data-toggle="collapse" href="#collapseThree" aria-expanded="true"
                              aria-controls="collapseThree">
                            Zoradiť články:
                        </span>
                    </div>

                    <div id="collapseThree" class="collapse show" role="tabpanel" aria-labelledby="headingThree">
                        <div class="card-body use-card">

                            @if(@isset($sort))
                                <select class="custom-select" name="sort" form="sortform">
                                    <option {{ $sort == 'DESC' ? 'selected' : '' }} value="DESC">Od najnovších</option>
                                    <option {{ $sort == 'ASC' ? 'selected' : '' }} value="ASC">Od najstarších</option>
                                </select>
                            @else
                                <select class="custom-select" name="sort" form="sortform">
                                    <option value="DESC">Od najnovších</option>
                                    <option value="ASC">Od najstarších</option>
                                </select>
                            @endif


                            <form id="sortform" action="{{route('sort')}}" method="get">
{{--                                {{ csrf_field() }}--}}
                                <input name="url" type="hidden" value="{{$url}}">
                                <input name="minutes_url" type="hidden" value="{{$minutes_url}}">
                                <button type="submit" class="btn link-btn use-btn">Zoradiť</button>
                            </form>
                        </div>
                    </div>
                </div>


                @if(@isset($minutes))

                    <div class="card">
                        <div class="card-header" role="tab" id="headingOne">
                                <img id="rss-img" style="max-width: 10%; margin-right: 5px" src="https://lh3.googleusercontent.com/g2_KDlWLLrW54uPG82zL20S24GVobubC-8h1rbSZbKubm2vdLUdvfsrzNPfsaagqVAZG">

                        <span class="custom" id="toogler" data-toggle="collapse" href="#collapseOne" aria-expanded="true"
                              aria-controls="collapseOne">
                            Minúta po minúte
                        </span>
                        </div>

                        <div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne">
                            <div class="card-body">
                                <div class="list-group">
                                        @foreach ($minutes['items'] as $item)
                                        <a href="{{$item['link']}}" target="_blank" class="list-group-item list-group-item-action flex-column align-items-start">
                                            <div class="d-flex w-100 justify-content-between">
                                                <h5 class="mb-1">{{$item['headline']}}</h5>
                                                <small style="margin-left: 5px">{{$item['day']}} {{$item['time']}}</small>
                                            </div>

                                            <img class="card-img-top card-img" src="{{$item['enclosure::url']}}" alt="">
                                        </a>

{{--                                           <a target="_blank" href="{{$item['link']}}" class="list-group-item">{{$item['day']}} {{$item['time']}} {{$item['headline']}}</a>--}}
                                        @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                @else
                    <div class="card">
                        <div class="card-header" role="tab" id="headingOne">
                        <span class="custom" id="toogler" data-toggle="collapse" href="#collapseOne" aria-expanded="true"
                              aria-controls="collapseOne">
                            Zoznam článkov:
                        </span>
                        </div>

                        <div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne">
                            <div class="card-body">
                                <div class="list-group">
                                    @if(@isset($rss['items']))
                                        @foreach ($rss['items'] as $item)
                                            <a target="_blank" href="{{$item['link']}}" class="list-group-item">{{$item['title']}}</a>
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                @endif



            @endisset


        </div>
        @isset($rss)
            <div class="col-12 col-md-6">



                <div class="card center-card">

                    @isset($rss['image'])
                        <div class="img-wrapper">
                            <img id="rss-img" src="{{$rss['image']}}">
                        </div>
                    @endisset


                    <h2 class="card-header">{{$rss['title']}}</h2>
                    <div class="card-body">
                        <p class="card-text">{{$rss['description']}}</p>
                    </div>
                </div>

                @if(@isset($rss['items']))
                    @foreach ($rss['items'] as $item)
                        <div class="card">


                            @if(@isset($item['enclosure::url']))
                            <img class="card-img-top card-img" src="{{$item['enclosure::url']}}" alt="">
                            @else
                            <img class="card-img-top card-img" src="{{$item['image']}}" alt="">
                            @endif
                            <div class="card-body">
                                <h4 class="card-title">{{$item['title']}}</h4>
                                <p class="card-text">
                                    {{$item['description']}}
                                </p>
                                <a target="_blank" href="{{$item['link']}}" class="btn link-btn">Celý článok</a>
                                <p class="public-date">Publikované: {{$item['pubDate']}}</p>
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>
        @endisset
    </div>
@endsection
