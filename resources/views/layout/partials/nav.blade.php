<nav class="navbar navbar-expand-sm navbar-dark">
    <a class="navbar-brand" href="/"><i class="fas fa-newspaper"></i> News Feed</a>

    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <i class="navbar-toggler-icon"></i>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav ml-auto">

            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    ESPN
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="/parse?url=https://www.espn.com/espn/rss/news">Top</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="/parse?url=https://www.espn.com/espn/rss/mlb/news"><i class="fas fa-baseball-ball"></i> MLB</a>
                    <a class="dropdown-item" href="/parse?url=https://www.espn.com/espn/rss/nfl/news"><i class="fas fa-football-ball"></i> NFL</a>
                    <a class="dropdown-item" href="/parse?url=https://www.espn.com/espn/rss/nba/news"><i class="fas fa-basketball-ball"></i> NBA</a>
                    <a class="dropdown-item" href="/parse?url=https://www.espn.com/espn/rss/nhl/news"><i class="fas fa-hockey-puck"></i> NHL</a>
                </div>
            </li>


            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Topky
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="/parse?url=https://www.topky.sk/rss/8/Topky/">Všetko</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="/parse?url=https://www.topky.sk/rss/10/Spravy_-_Domace.rss">Domáce</a>
                    <a class="dropdown-item" href="/parse?url=https://www.topky.sk/rss/10/Spravy_-_Domace.rss">Zahraničné</a>
                    <a class="dropdown-item" href="/parse?url=https://www.topky.sk/rss/5/Kultura.rss">Kultúra</a>
                    <a class="dropdown-item" href="/parse?url=https://www.topky.sk/rss/15/Prominenti.rss">Prominenti</a>
                    <a class="dropdown-item" href="/parse?url=https://www.topky.sk/rss/13/Zaujimavosti.rss">Zaujimavosti</a>

                </div>
            </li>

            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Denník N
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="/parse?url=https://dennikn.sk/feed&cat=2386">Všetko</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="/parse?url=https://dennikn.sk/slovensko/feed/&cat=430">Slovensko</a>
                    <a class="dropdown-item" href="/parse?url=https://dennikn.sk/svet/feed&cat=431">Svet</a>
                    <a class="dropdown-item" href="/parse?url=https://dennikn.sk/ekonomika/feed&cat=432">Ekonomika</a>
                    <a class="dropdown-item" href="/parse?url=https://dennikn.sk/komentare/feed">Komentáre</a>
                    <a class="dropdown-item" href="/parse?url=https://dennikn.sk/kultura/feed&cat=433">Kultúra</a>
                    <a class="dropdown-item" href="/parse?url=https://dennikn.sk/veda/feed&cat=508">Technológie a veda</a>
                    <a class="dropdown-item" href="/parse?url=https://dennikn.sk/sport/feed&cat=545">Šport</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="/parse?url=https://dennikn.sk/komentare/shooty/feed/">Karikatúry od Shootyho</a>

                </div>
            </li>

            <li class="nav-item">
                <a class="nav-link" href="/parse?url=https://www.sme.sk/rss-title">SME</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/parse?url=https://spravy.pravda.sk/rss/xml/">Pravda</a>
            </li>


        </ul>
    </div>


</nav>
