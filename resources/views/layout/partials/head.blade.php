<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<link href="{{ asset('css/index.css') }}" rel="stylesheet">

<link rel="apple-touch-icon" sizes="180x180" href="{{ asset('images/favicon_io/apple-touch-icon.png') }}">
<link rel="icon" type="image/png" sizes="32x32" href="{{ asset('images/favicon_io/favicon-32x32.png') }}">
<link rel="icon" type="image/png" sizes="32x32" href="{{ asset('images/favicon_io/favicon-16x16.png') }}">
<link rel="manifest" href="{{ asset('images/favicon_io/site.webmanifest') }}">
