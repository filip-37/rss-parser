<!doctype html>
<html lang="sk">
<head>
    @include('layout.partials.head')
    <title>News Feed</title>
</head>

<body>
@include('layout.partials.nav')

<main class="container">
    @yield('content')

</main>
@include('layout.partials.footer')

<!-- Bootstrap core JavaScript -->
@include('layout.partials.footer-scripts')
</body>
</html>



