<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Laravie\Parser\InvalidContentException;
use Orchestra\Parser\Xml\Facade as XmlParser;
use DateTime;

class ParseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('index');
    }

    public function parse($xml){
        return $xml->parse([
            'title' => ['uses' => 'channel.title'],
            'link' => ['uses' => 'channel.link'],
            'description' => ['uses' => 'channel.description'],
            'webMaster' => ['uses' => 'channel.webMaster'],
            'image' => ['uses' => 'channel.image[url]'],
            'items' => ['uses' => 'channel.item[title,description,pubDate,enclosure::url,link,image,content::url]'],
        ]);
    }

    public function parse_minutes($xml){
        return $xml->parse([
            'title' => ['uses' => 'channel.title'],
            'link' => ['uses' => 'channel.link'],
            'description' => ['uses' => 'channel.description'],
            'items' => ['uses' => 'channel.item[title,description,pubDate,enclosure::url,link]'],
        ]);
    }

    public function dateFormatter($date){

        $date = date_parse($date);

        if($date['day'] < 10)
            $date['day'] = '0'.$date['day'];
        if($date['month'] < 10)
            $date['month'] = '0'.$date['month'];
        if($date['hour'] < 10)
            $date['hour'] = '0'.$date['hour'];
        if($date['minute'] < 10)
            $date['minute'] = '0'.$date['minute'];

        return $date['day'].".".$date['month'].".".$date['year']." ".$date['hour'].":".$date['minute'];
    }

    public function load(Request $request)
    {
        $error = true;

        if ($request->input('url') == null) {
            return view('index')
                ->with('err', $error);
        }

        try {
            $xml = XmlParser::load($request->input('url'));
        } catch (InvalidContentException $ex) {
            return view('index')
                ->with('err', $error);
        }

        $rss = $this->parse($xml);

        $compare_function_DESC = function ($a, $b) {
            $a_timestamp = strtotime($a['pubDate']);
            $b_timestamp = strtotime($b['pubDate']);
            return $a_timestamp < $b_timestamp;
        };

        if ($rss['image'] != null)
            $rss['image'] = $rss['image'][0]['url'];

        if ($rss['items'] != null) {
            usort($rss['items'], $compare_function_DESC);

            for ($i = 0; $i < count($rss['items']); $i++) {
                $rss['items'][$i]['description'] = html_entity_decode($rss['items'][$i]['description']);
                $rss['items'][$i]['pubDate'] = $this->dateFormatter($rss['items'][$i]['pubDate']);
            }
        }

        /* MINUTES */


        $minutes = null;
        $minutes_url = null;
        if ($request->input('cat') != null){
        $minutes_url = "https://dennikn.sk/minuta/feed/?cat=" . $request->input('cat');
        $minutes = $this->load_minutes($minutes_url);
        }

        return view('index')
            ->with('rss', $rss)
            ->with('url', $request->input('url'))
            ->with('minutes', $minutes)
            ->with('minutes_url', $minutes_url);
    }

    public function load_minutes($url){


        try {
            $xml = XmlParser::load($url);
        } catch (InvalidContentException $ex) {
            return view('index')
                ->with('err', true);
        }
        $rss = $this->parse_minutes($xml);

        if ($rss['items'] != null) {
            for ($i = 0; $i < count($rss['items']); $i++) {
                $rss['items'][$i]['description'] = html_entity_decode($rss['items'][$i]['description']);


                $timestamp = strtotime($rss['items'][$i]['pubDate']);
                $day = date('d M',$timestamp);
                $timestamp = date("Y.m.d\\TH:i", $timestamp);

                $time = date("H:i", strtotime($rss['items'][$i]['pubDate'] . '+2 hours'));

                $today = new DateTime();
                $today->setTime( 0, 0, 0 );

                $match_date = DateTime::createFromFormat( "Y.m.d\\TH:i", $timestamp);
                $match_date->setTime( 0, 0, 0 );

                $diff = $today->diff( $match_date );
                $diffDays = (integer)$diff->format( "%R%a" );


                switch( $diffDays ) {
                    case 0:
                        $day = 'Dnes';
                        break;
                    case -1:
                        $day = 'Včera';
                        break;
                }


                $rss['items'][$i]['headline'] = preg_replace('/([^?!.]*.).*/', '\\1', $rss['items'][$i]['description']);
                $rss['items'][$i]['day'] = $day;
                $rss['items'][$i]['time'] = $time;
            }
        }
        return $rss;
    }

    public function sort(Request $request)
    {

        $xml = XmlParser::load($request->input('url'));
        $rss = $this->parse($xml);

        $array = $rss['items'];

        $compare_function_ASC = function ($a, $b) {
            $a_timestamp = strtotime($a['pubDate']);
            $b_timestamp = strtotime($b['pubDate']);
            return $a_timestamp > $b_timestamp;
        };

        $compare_function_DESC = function ($a, $b) {
            $a_timestamp = strtotime($a['pubDate']);
            $b_timestamp = strtotime($b['pubDate']);
            return $a_timestamp < $b_timestamp;
        };

        if ($rss['items'] != null) {

            if ($request->input('sort') == 'ASC')
                usort($array, $compare_function_ASC);
            else
                usort($array, $compare_function_DESC);

            $rss['items'] = $array;

            for ($i = 0; $i < count($rss['items']); $i++){
                $rss['items'][$i]['description'] = html_entity_decode($rss['items'][$i]['description']);
                $rss['items'][$i]['pubDate'] = $this->dateFormatter($rss['items'][$i]['pubDate']);
            }

        }
        if ($rss['image'] != null)
            $rss['image'] = $rss['image'][0]['url'];

        /* MINUTES */
        $minutes = null;
        $minutes_url = null;
        if ($request->input('minutes_url') != null){
            $minutes_url = $request->input('minutes_url');
            $minutes = $this->load_minutes($minutes_url);

            $array = $minutes['items'];
            if ($request->input('sort') == 'ASC')
                usort($array, $compare_function_ASC);
            else
                usort($array, $compare_function_DESC);
            $minutes['items'] = $array;
        }



        return view('index')
            ->with('rss', $rss)
            ->with('url', $request->input('url'))
            ->with('sort', $request->input('sort'))
            ->with('minutes', $minutes)
            ->with('minutes_url', $minutes_url);
    }
}
